package com.Controller;

import com.Business.CityBusiness;
import com.Business.FlightBusiness;
import com.model.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    FlightBusiness flightBusiness;

    @Autowired
    CityBusiness cityBusiness;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView getHomepage(ModelMap map) {
        map.addAttribute("flights", flightBusiness.getAll());
        map.addAttribute("cities", cityBusiness.getAll());
        return new ModelAndView("admin", map);

    }


    @RequestMapping(value = "/deleteFlight", method = RequestMethod.GET)
    public ModelAndView getLocalTime(@RequestParam String flightNumber, ModelMap map) {
        if (flightBusiness.delete(flightNumber)) {
            map.addAttribute("responseMessage", "Flight #" + flightNumber + " deleted");
        } else {
            map.addAttribute("responseMessage", "Flight #" + flightNumber + " not found");
        }
        map.addAttribute("flights", flightBusiness.getAll());
        map.addAttribute("cities", cityBusiness.getAll());
        return new ModelAndView("homepage", map);

    }


    @RequestMapping(value = "/addFlight", method = RequestMethod.POST)
    public ModelAndView addFlight(
            @RequestParam String flightNumber,
            @RequestParam String arrivalCity,
            @RequestParam String airplaneType,
            @RequestParam String departureCity,
            ModelMap map,
            HttpServletRequest request) throws ParseException {
        Flight newFlight = flightBusiness.checkExisting(flightNumber);

        if (newFlight == null) {
            newFlight = new Flight();
        }


        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        newFlight.setDepartureDate(formatter.parse(request.getParameter("departureTime")));
        newFlight.setArrivalDate(formatter.parse(request.getParameter("arrivalTime")));

        newFlight.setArrivalCity(cityBusiness.getCity(arrivalCity));
        newFlight.setDepartureCity(cityBusiness.getCity(departureCity));
        newFlight.setAirplaneType(airplaneType);
        newFlight.setFlightNumber(flightNumber);

        flightBusiness.save(newFlight);

        map.addAttribute("flights", flightBusiness.getAll());
        map.addAttribute("cities", cityBusiness.getAll());
        return new ModelAndView("admin", map);

    }
}
