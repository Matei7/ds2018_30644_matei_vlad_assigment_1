package com.Controller;

import com.Business.CityBusiness;
import com.Business.FlightBusiness;
import com.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {
    @Autowired
    FlightBusiness flightBusiness;
    @Autowired
    CityBusiness cityBusiness;

    @RequestMapping(value = "/homepage", method = RequestMethod.GET)
    public ModelAndView getHomepage(ModelMap map) {
        map.addAttribute("flights", flightBusiness.getAll());
        return new ModelAndView("homepage", map);

    }


    @RequestMapping(value = "/getLocalTime", method = RequestMethod.GET)
    public ModelAndView getLocalTime(@RequestParam String cityName, ModelMap map) {
        map.addAttribute("localTime", cityName + " local time is: " + cityBusiness.getLocalTime(cityName));
        map.addAttribute("flights", flightBusiness.getAll());
        return new ModelAndView("homepage", map);

    }


}
