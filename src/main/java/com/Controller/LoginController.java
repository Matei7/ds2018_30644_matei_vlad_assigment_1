package com.Controller;


import com.Business.CityBusiness;
import com.Business.FlightBusiness;
import com.Business.UserBusiness;

import com.DAO.CityDAO;
import com.DAO.FlightDAO;
import com.DAO.UserDAO;
import com.model.City;
import com.model.Flight;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

@Controller
public class LoginController {
    @Autowired
    UserBusiness userBusiness;
    @Autowired
    FlightBusiness flightBusiness;

    @Autowired
    CityBusiness cityBusiness;


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage() {
        return "login";
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView handleLoginRequest(@RequestParam String username, @RequestParam String password, ModelMap model, HttpServletRequest request) throws UnsupportedEncodingException {
        User user = userBusiness.existingUser(username, password);

        if (user != null) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            if (user.getRole().equals("user")) {
                model.addAttribute("flights", flightBusiness.getAll());
                return new ModelAndView("homepage", model);
            } else {
                model.addAttribute("flights", flightBusiness.getAll());
                model.addAttribute("cities", cityBusiness.getAll());
                return new ModelAndView("admin", model);
            }
        }

        model.addAttribute("error", "Invalid username/password");
        return new ModelAndView("login", model);

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse httpResponse) throws IOException {

        HttpSession session = request.getSession(false);
        session.removeAttribute("user");
        session.invalidate();

        httpResponse.sendRedirect("/login");
    }


    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String addUser(@RequestParam String username, @RequestParam String password,
                          @RequestParam String name, @RequestParam String email
    ) throws UnsupportedEncodingException {

        userBusiness.addUser(username, password, name, email, "user");
        return "login";
    }


    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String showCreateAccount() {
        return "register";
    }

    @RequestMapping(value = "addAdmin", method = RequestMethod.POST)
    public ModelAndView addAdmin(@RequestParam String username, @RequestParam String password,
                                 @RequestParam String name, @RequestParam String email,
                                 ModelMap model) throws UnsupportedEncodingException {

        userBusiness.addUser(username, password, name, email, "admin");

        return new ModelAndView("homepage2", model);
    }

    @RequestMapping(value = "addAdmin", method = RequestMethod.GET)
    public String addAdminPage() {
        return "addAdmin";
    }

}
