package com.Business;

import com.DAO.FlightDAO;
import com.model.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightBusiness {
    @Autowired
    FlightDAO flightDAO;

    public List<Flight> getAll() {
        return (List<Flight>) flightDAO.findAll();
    }

    public void save(Flight newFlight) {
        flightDAO.save(newFlight);
    }

    public boolean delete(String flightNumber) {
        Flight flight = flightDAO.findByFlightNumber(flightNumber).get(0);
        if (flight != null) {
            flightDAO.delete(flight);
            return true;
        }
        return false;
    }

    public Flight checkExisting(String flightNumber) {
        return flightDAO.findByFlightNumber(flightNumber).get(0);
    }
}
