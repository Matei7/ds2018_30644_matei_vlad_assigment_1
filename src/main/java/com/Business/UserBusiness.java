package com.Business;


import com.DAO.UserDAO;
import com.Service.PasswordHash;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;


@Service
public class UserBusiness {
    @Autowired
    UserDAO userRep;
    @Autowired
    PasswordHash passHash;


    public void addUser(String username, String password, String name, String email, String role) throws UnsupportedEncodingException {
        User userToBeAdded = new User();
        userToBeAdded.setUsername(username);
        userToBeAdded.setPassword(passHash.getSha512SecurePassword(password, username.toLowerCase()));
        userToBeAdded.setName(name);
        userToBeAdded.setEmail(email);
        userToBeAdded.setRole(role);
        userRep.save(userToBeAdded);
    }

    public User existingUser(String username, String password) {
        List<User> user = null;
        try {
            user = userRep.findByUsernameAndPassword(username, passHash.getSha512SecurePassword(password, username.toLowerCase()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (user.size() > 0) {
            return user.get(0);
        }
        return null;
    }
}
