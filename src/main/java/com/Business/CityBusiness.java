package com.Business;

import com.DAO.CityDAO;
import com.model.City;
import com.model.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CityBusiness {
    @Autowired
    CityDAO cityDAO;

    public String getLocalTime(String cityName) {
        City city = cityDAO.findByName(cityName).get(0);

        RestTemplate restTemplate = new RestTemplate();
        LocalTime localTime = restTemplate.getForObject("http://api.timezonedb.com/v2.1/get-time-zone?key=149U3QRZNFQ1&format=json&by=position&lat=" + city.getLatitude() + "&lng=" + city.getLongitude(), LocalTime.class);

        return localTime.getFormatted();
    }

    public List<City> getAll() {
        return (List<City>) cityDAO.findAll();
    }

    public City getCity(String cityName) {
        return cityDAO.findByName(cityName).get(0);
    }
}
