package com.DAO;

import com.model.City;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityDAO extends CrudRepository<City, Integer> {

    List<City> findByName(String cityName);
}
