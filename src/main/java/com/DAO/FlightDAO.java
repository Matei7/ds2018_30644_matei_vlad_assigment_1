package com.DAO;

import com.model.Flight;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FlightDAO  extends CrudRepository<Flight, Integer> {

    List<Flight> findByFlightNumber(String flightNumber);
}
