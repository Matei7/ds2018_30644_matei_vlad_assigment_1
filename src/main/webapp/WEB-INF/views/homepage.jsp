<%@ page language="java" contentType="text/html" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<html>
<head>
    <style type="text/css">


        input.invalid, textarea.invalid {
            border: 2px solid red;
        }

        input.valid, textarea.valid {
            border: 2px solid green;
        }

        header, footer {
            padding: 1em;
            color: white;
            background-color: black;
            clear: left;
            text-align: center;
        }

        header {
            background-image: linear-gradient(black, white);
        }

        footer {
            background-image: linear-gradient(white, black);
            height: 120;
        }

        nav {
            float: left;
            max-width: 400px;
            margin: 0;
            padding: 1em;
        }

        body {

        }

        article {
            padding: 1em;
            overflow: hidden;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 20px;
            text-align: justify;
        }

        .button span {
            cursor: pointer;
            display: inline-block;
            position: relative;
            transition: 0.5s;
        }

        .logOut {
            margin-left: 20px;
            font-size: 16px;
            border: 2px solid #4CAF50;
            display: block;
            background-color: black;
            border-radius: 10%;
        }

        .homepageButton {
            display: inline-block;
            float: left;
            width: 100px;
            font-size: 16px;
            border: 2px solid #4CAF50;
            display: block;
            background-color: black;
            border-radius: 10%;
        }

        .button span:after {
            content: '\00bb';
            position: absolute;
            opacity: 0;
            top: 0;
            right: -20px;
            transition: 0.5s;
        }

        .button:hover span {
            padding-right: 25px;
        }

        .button:hover span:after {
            opacity: 1;
            right: 0;
        }

        .css-serial {
            counter-reset: serial-number; /* Set the serial number counter to 0 */
        }

        .css-serial td:first-child:before {
            counter-increment: serial-number; /* Increment the serial number counter */
            content: counter(serial-number); /* Display the counter */
        }


    </style>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <header>
        <h1>Flights App </h1>
        <form action="/homepage" method="GET">
            <button  type="submit" class="homepageButton">Homepage</button>
        </form>
        <form action="/logout" method="GET">
            <p align="right">
                <button type="submit" class="logOut">Logout</button>
            </p>
        </form>


    </header>


    <article>
        <form class="login-form" action="/getLocalTime" method="GET">
            <input name="cityName" type="text" placeholder="City Name" required/>
            <button type="submit">Get Local Time</button>

            <c:if test="${not empty localTime}">
                <p align="center">${localTime}</p>
                <br>
                <br>
            </c:if>
        </form>

        <table class="table table-striped table-bordered">
            <thead>
            <th>Flight Number</th>
            <th>Airplane Type</th>
            <th>Departure City</th>
            <th>Departure date</th>
            <th>Arrival City</th>
            <th>Arrival date</th>
            </thead>
            <c:forEach items="${flights}" var="flight">
                <tr>

                    <td>${flight.flightNumber}</td>
                    <td>${flight.airplaneType}</td>
                    <td>${flight.departureCity.getName()}</td>
                    <td>${flight.departureDate}</td>
                    <td>${flight.arrivalCity.getName()}</td>
                    <td>${flight.arrivalDate}</td>

                </tr>
            </c:forEach>
        </table>
    </article>
    <footer></footer>
</div>
</body>
</html>