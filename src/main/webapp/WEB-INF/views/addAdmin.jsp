<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>

    <%@include file="createAccBody.html" %>
    <title>Create Account</title>
</head>

<body style="background-color:#76b852;">


<article>

    <form action="/addAdmin" method="POST">
        <h1>PolyBet App </h1>
        <div>
            <h2>Sign Up</h2>

            <p>
                <label for="name" class="floatLabel">Full Name</label>
                <input id="name" name="name" type="text" required=''>
            </p>

            <p>
                <label for="username" class="floatLabel">Username</label>
                <input id="username" name="username" type="text" required=''>
            </p>
            <p>
                <label for="password" class="floatLabel">Password</label>
                <input id="password" name="password" type="password" required=''>
            </p>


            <p>
                <label for="email" class="floatLabel">Email</label>
                <input id="email" name="email" type="text" required=''>
            </p>


            <p>
                <input type="submit" value="Create My Account" id="submit">
            </p>

        </div>
    </form>

</article>



</body>
</html>