<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>

    <%@include file="createAccBody.html" %>
    <title>Create Account</title>
</head>

<body style="background-color:#76b852;">


<article>
    <p style="color:red;" align="center">${errorAccount}</p>


    <form action="/addUser" method="POST">
        <h1>PolyBet App </h1>
        <div>
            <h2>Sign Up</h2>

            <p>
                <label for="name" class="floatLabel">Full Name</label>
                <input id="name" name="name" type="text">
            </p>

            <p>
                <label for="username" class="floatLabel">Username</label>
                <input id="username" name="username" type="text">
            </p>
            <p>
                <label for="password" class="floatLabel">Password</label>
                <input id="password" name="password" type="password">
            </p>

            <p>
                <label for="addr" class="floatLabel">Address</label>
                <input id="addr" name="addr" type="text"/>
            </p>

            <p>
                <label for="email" class="floatLabel">Email</label>
                <input id="email" name="email" type="text">
            </p>


            <p>
                <label for="cellphone" class="floatLabel">Cellphone</label>
                <input id="cellphone" name="cellphone" type="text">
            </p>

            <p>
                <input type="submit" value="Create My Account" id="submit">
            </p>

        </div>
    </form>

</article>



</body>
</html>