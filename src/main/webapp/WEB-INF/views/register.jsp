<html>
<head>

    <style type="text/css">
        body {
            background: #76b852;
            font-family: sans-serif;
            font-size: 10px;
        }

        form {
            background: #fff;
            padding: 4em 4em 2em;
            max-width: 700px;
            margin: 50px auto 0;
            box-shadow: 0 0 1em #222;
            border-radius: 2px;
        }
        form h2 {
            margin: 0 0 50px 0;
            padding: 10px;
            text-align: center;
            font-size: 30px;
            color: #666666;
            border-bottom: solid 1px #e5e5e5;
        }

        form h1 {
            margin: 0 0 25px 0;
            padding: 10px;
            text-align: center;
            font-size: 30px;
            color: #666666;
        }

        form p {
            margin: 0 0 3em 0;
            position: relative;
        }
        form input {
            display: block;
            box-sizing: border-box;
            width: 100%;
            outline: none;
            margin: 0;
        }
        form input[type="text"],
        form input[type="password"] {
            background: #fff;
            border: 1px solid #dbdbdb;
            font-size: 1.6em;
            padding: .8em .5em;
            border-radius: 2px;
        }
        form input[type="text"]:focus,
        form input[type="password"]:focus {
            background: #fff;
        }
        form span {
            display: block;
            background: #F9A5A5;
            padding: 2px 5px;
            color: #666;
        }
        form input[type="submit"] {
            background: #4CAF50;
            box-shadow: 0 3px 0 0 rgba(123, 163, 73, 0.7);
            border-radius: 2px;
            border: none;
            color: #fff;
            cursor: pointer;
            display: block;
            font-size: 2em;
            line-height: 1.6em;
            margin: 2em 0 0;
            outline: none;
            padding: .8em 0;
            text-shadow: 0 1px #68B25B;
        }
        form input[type="submit"]:hover {
            background: #43A047;
            text-shadow: 0 1px 3px rgba(70, 93, 41, 0.7);
        }
        form label {
            position: absolute;
            left: 8px;
            top: 12px;
            color: #999;
            font-size: 16px;
            display: inline-block;
            padding: 4px 10px;
            font-weight: 400;
            background-color: rgba(255, 255, 255, 0);
            -moz-transition: color 0.3s, top 0.3s, background-color 0.8s;
            -o-transition: color 0.3s, top 0.3s, background-color 0.8s;
            -webkit-transition: color 0.3s, top 0.3s, background-color 0.8s;
            transition: color 0.3s, top 0.3s, background-color 0.8s;
        }
        form label.floatLabel {
            top: -11px;
            background-color: rgba(255, 255, 255, 0.8);
            font-size: 14px;
        }

    </style>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Create Account</title>
</head>

<body style="background-color:#76b852;">


<article>
        <form action="/register" method="POST">
        <h1>Flights App </h1>
        <div>
            <h2>Sign Up</h2>

            <p>
                <label for="name" class="floatLabel">Full Name</label>
                <input id="name" name="name" type="text" required=''>
            </p>

            <p>
                <label for="username" class="floatLabel">Username</label>
                <input id="username" name="username" type="text" required=''>
            </p>
            <p>
                <label for="password" class="floatLabel">Password</label>
                <input id="password" name="password" type="password" required=''>
            </p>

            <p>
                <label for="email" class="floatLabel">Email</label>
                <input id="email" name="email" type="text" required=''>
            </p>


            <p>
                <input type="submit" value="Create My Account" id="submit">
            </p>

        </div>
    </form>

</article>



</body>
</html>